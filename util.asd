;;;; -*- Mode: LISP; Syntax: ANSI-Common-Lisp; Base: 10 -*-

(in-package asdf)

(defpackage "ZPB-TTF"
  (:export "OPEN-FONT-LOADER-FROM-STREAM"
           "*DUMP-CHARACTER-LIST*"
           "DUMP-FONT-LOADER-TO-STREAM"))

(defsystem :zpb-ttf-util
  :name "zpb-ttf-util"
  :author "KURODA Hisao <littlelisper@gmail.com>"
  :maintainer "KURODA Hisao <littlelisper@gmail.com>"
  :description "ZPB-TTF Util"
  :long-description "ZPB-TTF Util"
  :components ((:file "util" :depends-on ()))
  :depends-on (:zpb-ttf))
