;;; -*- coding:cp932; syntax:common-lisp -*-

(defpackage "ZPB-TTF"
  (:export "OPEN-FONT-LOADER-FROM-STREAM"
           "*DUMP-CHARACTER-LIST*"
           "DUMP-FONT-LOADER-TO-STREAM"))

(in-package "ZPB-TTF")

#||
;;; cmap encoding subtable: platformID, platformSpecificID and offset
;;; fo·Ζ«Ν UCS-2 Μe[uΎ―ΕD’?
(0 3 3976) ; Default semantics & PRC
(3 1 60042) ; Unicode 2.0 or later semantics (BMP only) & UCS-2
(3 10 116108) ; Unicode 2.0 or later semantics (BMP only) & UCS-4

;;; Glyph Internal
(with-open-file (input "c:/Windows/Fonts/ipam.ttf" :external-format :octets)
  (loop for char in '(#\ #\  #\‘ #\’ #\ζ)
      do (file-position input
                        (zpb-ttf::location (zpb-ttf:find-glyph char fontloader)))
         (print (list char 
                      (zpb-ttf::read-int16 input) ; number-of-contours
                      (zpb-ttf::read-int16 input) ; xmin
                      (zpb-ttf::read-int16 input) ; ymin
                      (zpb-ttf::read-int16 input) ; xmax
                      (zpb-ttf::read-int16 input) ; ymax
                      ))))
(#\ 3 369 -115 1717 1360) 
(#\  3 244 -74 1825 1661) 
(#\‘ 2 324 23 1786 1135) 
(#\’ 2 170 82 1862 1374) 
(#\ζ 7 121 -148 1928 1698) 

;;;
(with-open-file (input "c:/Windows/Fonts/ipam.ttf" :external-format :octets)
  (let ((loader (open-font-loader-from-stream input)))
    (loop for char in '(#\1 #\a #\ #\  #\‘ #\’ #\ζ)
        for glyph = (find-glyph char font-loader)
        for index = (font-index glyph)
        collect (list char (read-contours-at-index index loader)))))

;;; Copy TTF Font File
(with-open-file (input "c:/Windows/Fonts/ipam.ttf" :external-format :octets)
  (with-open-file (output "foo" :direction :output :if-exists :supersede :external-format :octets)
    (let ((intro (make-sequence '(vector (unsigned-byte 8)) 300)))
      (read-sequence intro input)
      (write-sequence intro output))
    (loop for (name offset size) in '(("GDEF" 300 30)
                                      ("GSUB" 332 3518)
                                      ("OS/2" 3852 96)
                                      ("cmap" 3948 236166) ; t 2
                                      ("cvt " 240116 204)
                                      ("fpgm" 240320 113)
                                      ("gasp" 240436 16)
                                      ("glyf" 240452 7530768) ;t 1
                                      ("head" 7771220 54) ;t
                                      ("hhea" 7771276 36) ;t
                                      ("hmtx" 7771312 50600) ;t
                                      ("loca" 7821912 50916) ;t 4
                                      ("maxp" 7872828 32) ;t
                                      ("name" 7872860 2438) ;t
                                      ("post" 7875300 120451) ;t 3
                                      ("prep" 7995752 10) 
                                      ("vhea" 7995764 36) 
                                      ("vmtx" 7995800 50910))
        do (let ((seq (make-sequence '(vector (unsigned-byte 8)) size)))
             (file-position input offset)
             (file-position output offset)
             (read-sequence seq input)
             (write-sequence seq output)))
    (loop for byte = (read-byte input nil nil)
        while byte do
          (write-byte byte output))))

;;; IPAMincho ttf table: (ΌO offset size)
("GDEF" 300 30) 
("GSUB" 332 3518) 
("OS/2" 3852 96) 
("cmap" 3948 236166)                    ;t 2
("cvt " 240116 204) 
("fpgm" 240320 113) 
("gasp" 240436 16) 
("glyf" 240452 7530768)                 ;t 1
("head" 7771220 54)                     ;t
("hhea" 7771276 36)                     ;t
("hmtx" 7771312 50600)                  ;t
("loca" 7821912 50916)                  ;t 4
("maxp" 7872828 32)                     ;t
("name" 7872860 2438)                   ;t
("post" 7875300 120451)                 ;t 3
("prep" 7995752 10) 
("vhea" 7995764 36) 
("vmtx" 7995800 50910) 
||#


;;; Redefinitions

(defun open-font-loader-from-stream (input-stream)
  (let ((magic (read-uint32 input-stream)))
    (when (/= magic #x00010000 #x74727565)
      (error 'bad-magic
             :location "font header"
             :expected-values (list #x00010000 #x74727565)
             :actual-value magic))
    (let* ((table-count (read-uint16 input-stream))
           (search-range (read-uint16 input-stream))
           (entry-selector (read-uint16 input-stream))
           (range-shift (read-uint16 input-stream))
           (font-loader (make-instance 'font-loader
                                       :input-stream input-stream
                                       :scaler-type magic
                                       :search-range search-range
                                       :entry-selector entry-selector
                                       :range-shift range-shift
                                       :table-count table-count)))
      ;;uint32 CalcTableChecksum(uint32 *table, uint32 numberOfBytesInTable)
      ;;    {
      ;;    uint32 sum = 0;
      ;;    uint32 nLongs = (numberOfBytesInTable + 3) / 4;
      ;;    while (nLongs-- > 0)
      ;;        sum += *table++;
      ;;    return sum;
      ;;    }
      (loop repeat table-count
            for tag = (read-uint32 input-stream)
            for checksum = (read-uint32 input-stream)
            for offset = (read-uint32 input-stream)
            for size = (read-uint32 input-stream)
            do (setf (gethash tag (tables font-loader))
                     (make-instance 'table-info
                                    :checksum checksum
                                    :offset offset
                                    :name (number->tag tag)
                                    :size size)))
      (load-maxp-info font-loader)
      (load-gdef-info font-loader)      ; new
      (load-gsub-info font-loader)      ; new
      (load-os/2-info font-loader)      ; new
      (load-cmap-info font-loader)
      (load-cvt--info font-loader)      ; new
      (load-fpgm-info font-loader)      ; new
      (load-gasp-info font-loader)      ; new
      (load-head-info font-loader)
      (load-hhea-info font-loader)
      (load-hmtx-info font-loader)
      (load-kern-info font-loader)      ; No Kern in IPAMincho
      (load-loca-info font-loader)
      (load-name-info font-loader)
      (load-post-info font-loader)
      (load-prep-info font-loader)      ; new
      (load-vhea-info font-loader)      ; new
      (load-vmtx-info font-loader)      ; new
      (load-glyf-info font-loader)
      (setf (glyph-cache font-loader)
            (make-array (glyph-count font-loader) :initial-element nil))
      (loop for table-info being the hash-values of (tables font-loader)
          for offset = (offset table-info)
          for name = (name table-info)
          for size = (size table-info)
          do (print (list name offset size)))
      font-loader)))

#+ignore
(defparameter +ipa-mincho-table-name-offset-size+
    '(("GDEF" 300 30) 
      ("GSUB" 332 3518) 
      ("OS/2" 3852 96) 
      ("cmap" 3948 236166)              ;t 2
      ("cvt " 240116 204) 
      ("fpgm" 240320 113) 
      ("gasp" 240436 16) 
      ("glyf" 240452 7530768)           ;t 1
      ("head" 7771220 54)               ;t
      ("hhea" 7771276 36)               ;t
      ("hmtx" 7771312 50600)            ;t
      ("loca" 7821912 50916)            ;t 4
      ("maxp" 7872828 32)               ;t
      ("name" 7872860 2438)             ;t
      ("post" 7875300 120451)           ;t 3
      ("prep" 7995752 10) 
      ("vhea" 7995764 36) 
      ("vmtx" 7995800 50910)))

(defvar *dump-character-list*)

#+obsolete
(defun dump-font-loader-to-stream (font-loader output-stream)
  (let* ((magic (scaler-type font-loader))
         (table-list '("cvt " "fpgm" "glyf" "head" "hhea" "hmtx" "loca" "maxp" "post" "prep" "vhea" "vmtx"))
         (table-count (length table-list)) ; (table-count font-loader)
         (search-range (search-range font-loader))
         (entry-selector (entry-selector font-loader))
         (range-shift (range-shift font-loader)))
    (write-uint32 magic output-stream)
    (write-uint16 table-count output-stream)
    (write-uint16 search-range output-stream)
    (write-uint16 entry-selector output-stream)
    (write-uint16 range-shift output-stream)
    ;;uint32 CalcTableChecksum(uint32 *table, uint32 numberOfBytesInTable)
    ;;    {
    ;;    uint32 sum = 0;
    ;;    uint32 nLongs = (numberOfBytesInTable + 3) / 4;
    ;;    while (nLongs-- > 0)
    ;;        sum += *table++;
    ;;    return sum;
    ;;    }
    (let ((start-table (file-position output-stream)))
      (advance-file-position output-stream
                             (loop repeat (hash-table-count (tables font-loader))
                                 sum 16))
      ;; (dump-gdef-info font-loader output-stream)
      (change-table-size "GDEF" 0 font-loader)
      ;; (dump-gsub-info font-loader output-stream)
      (change-table-size "GSUB" 0 font-loader)
      ;; (dump-os/2-info font-loader output-stream)
      (change-table-size "OS/2" 0 font-loader)
      ;; (dump-cmap-info font-loader output-stream)
      (change-table-size "cmap" 0 font-loader)
      (dump-cvt--info font-loader output-stream)
      (dump-fpgm-info font-loader output-stream)
      ;; (dump-gasp-info font-loader output-stream)
      (change-table-size "gasp" 0 font-loader)
      (dump-glyf-info font-loader output-stream)
      (dump-head-info font-loader output-stream)
      (dump-hhea-info font-loader output-stream)
      (dump-hmtx-info font-loader output-stream)
      ;; (dump-kern-info font-loader output-stream)      ; No Kern in IPAMincho
      (dump-loca-info font-loader output-stream)
      (dump-maxp-info font-loader output-stream)
      ;; (dump-name-info font-loader output-stream)
      (change-table-size "name" 0 font-loader)
      (dump-post-info font-loader output-stream)
      (dump-prep-info font-loader output-stream)
      (dump-vhea-info font-loader output-stream)
      (dump-vmtx-info font-loader output-stream)
      #+ignore
      (loop ;; for table-info being the hash-values of (tables font-loader)
          for (name nil nil) in ipa-mincho-table-name-offset-size
          for table-info = (gethash (tag->number name) (tables font-loader))
          for table-position from start-table by 16
          for offset = (offset table-info)
          for size = (size table-info)
          for checksum = (calc-table-checksum offset size output-stream)
          do (print (list name offset size))
             (file-position output-stream table-position)
             (write-uint32 (tag->number (name table-info)) output-stream)
             (write-uint32 checksum output-stream)
             (write-uint32 offset output-stream)
             (write-uint32 size output-stream))
      (loop for name in table-list
          for table-info = (table-info name font-loader)
          for table-position from start-table by 16
          for offset = (offset table-info)
          for size = (size table-info)
          for checksum = (calc-table-checksum offset size output-stream)
          do (print (list name offset size))
             (file-position output-stream table-position)
             (write-uint32 (tag->number (name table-info)) output-stream)
             (write-uint32 checksum output-stream)
             (write-uint32 offset output-stream)
             (write-uint32 size output-stream)))))

(defun dump-font-loader-to-stream (font-loader output-stream &optional (table-name-list '("cvt " "fpgm" "glyf" "head" "hhea" "hmtx" "loca" "maxp" "post" "prep" "vhea" "vmtx")))
  (let* ((magic (scaler-type font-loader))
         (table-count (length table-name-list)) ; (table-count font-loader)
         (search-range (search-range font-loader))
         (entry-selector (entry-selector font-loader))
         (range-shift (range-shift font-loader)))
    (write-uint32 magic output-stream)
    (write-uint16 table-count output-stream)
    (write-uint16 search-range output-stream)
    (write-uint16 entry-selector output-stream)
    (write-uint16 range-shift output-stream)
    (let ((start-table (file-position output-stream))
          (table-offset (loop repeat (hash-table-count (tables font-loader))
                            sum 16))
          (tables (loop for table-info being the hash-values of (tables font-loader)
                      using (hash-key number)
                      collect (cons (number->tag number) table-info))))
      (advance-file-position output-stream table-offset)
      (let ((sorted-table-list
             (sort tables #'< :key #'(lambda (x) (offset (cdr x))))))
        (loop for (name . nil) in sorted-table-list
            do (dump-table-info name font-loader output-stream table-name-list))
        (loop for name in table-name-list
            for table-info = (table-info name font-loader)
            for table-position from start-table by 16
            for offset = (offset table-info)
            for size = (size table-info)
            for checksum = (calc-table-checksum offset size output-stream)
            do (print (list name offset size))
               (file-position output-stream table-position)
               (write-uint32 (tag->number (name table-info)) output-stream)
               (write-uint32 checksum output-stream)
               (write-uint32 offset output-stream)
               (write-uint32 size output-stream))))))
  
(defun dump-table-info (name font-loader output-stream &optional table-name-list)
  (cond ((not (member name table-name-list :test #'string=))
         (change-table-size name 0 font-loader))
        ((string= name "cvt ")
         (dump-cvt--info font-loader output-stream))
        ((string= name "fpgm")
         (dump-fpgm-info font-loader output-stream))
        ((string= name "hhea")
         (dump-hhea-info font-loader output-stream))
        ((string= name "maxp")
         (dump-maxp-info font-loader output-stream))
        ((string= name "post")
         (dump-post-info font-loader output-stream))
        ((string= name "prep")
         (dump-prep-info font-loader output-stream))
        ((string= name "vhea")
         (dump-vhea-info font-loader output-stream))
        ((string= name "glyf")
         (dump-glyf-info font-loader output-stream))
        ((string= name "loca")
         (dump-loca-info font-loader output-stream))
        ((string= name "hmtx")
         (dump-hmtx-info font-loader output-stream))
        ((string= name "vmtx")
         (dump-vmtx-info font-loader output-stream))
        ((string= name "head")
         (dump-head-info font-loader output-stream))
        ((string= name "cmap")
         (dump-cmap-info font-loader output-stream))
        ((string= name "name")
         (dump-name-info font-loader output-stream))
        ((string= name "gasp")
         (dump-gasp-info font-loader output-stream))
        (t
         (error "Unknown Table Name ~A." name))))

(progn
  (defparameter +sample-string-list+
      '(" !@#$%^&*()_+"
        "@IOijQ{b¨"
        "abcdefghijklmnopqrstuvwxyz"
        "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
        ""
        "`abcdefghijklmno"
        "0123456789-=`"
        "OPQRSTUVWX]"
        "²ΫΚΖΞΝΔήΑΨΗΩ¦"
        "’λΝΙΩΦΗΏθΚιπ"
        "νͺζ½κΌΒΛΘηή" 
        "€ξΜ¨­βά―Σ±¦Δ"
        " ³«δίέ΅οΠΰΉ·"))
  (defparameter +sample-character-list+
      (loop for string in sample-string-list
          append (loop for char across string collect char))))

#||

CL-USER(869): :cd ~/project/honda-2016/devel
CL-USER(870): :ld init
CL-USER(877): :ld zpb/defsystem.lisp
CL-USER(878): (excl:load-system :zpb-patch :compile t)
CL-USER(878): :pa :zpb-ttf
ZPB-TTF(882): (with-open-file (input "c:/Windows/Fonts/ipam.ttf" :external-format :octets)
                (open-font-loader-from-stream input))
ZPB-TTF(883): (setq font-loader *)
ZPB-TTF(884): (with-open-file (stream "foo" :direction :io :if-exists :supersede)
                (dump-font-loader-to-stream font-loader stream sample-character-list))
ZPB-TTF(888): :pa :pdf
CL-PDF(889): :cl ../sample-codes/pdf-parser/cl-pdf-unicode.lisp
CL-PDF(889): (remhash (list "ipamincho" (get-encoding *unicode-encoding*)) *font-cache*)
T
CL-PDF(890): (load-ipa-font "../devel/pdf/ipam.ufm" "foo")
#<TTU-FONT-METRICS IPAMincho @ #x1240f8392>
CL-PDF(891): (make-c2g-subset * sample-character-list)
CL-PDF(891): (jexample)
CL-PDF(944): (file-length "tmp.pdf")
122771
CL-PDF(945): (file-length "../sample-codes/pdf-parser/iroha.pdf")
8361590

;;;;;

CL-USER(245): zpb-ttf::(let ((*dump-character-list* sample-character-list))
                         (setq font-loader
                           (with-open-file (input "c:/Windows/Fonts/ipam.ttf" :external-format :octets)
                             (open-font-loader-from-stream input)))
                         (with-open-file (stream "foo" :direction :io :if-exists :supersede)
                           (dump-font-loader-to-stream font-loader stream)))
("cvt " 300 204) 
("fpgm" 504 113) 
("glyf" 620 41100) 
("head" 41720 54) 
("hhea" 41776 36) 
("hmtx" 41812 2716) 
("loca" 44528 25460) 
("maxp" 69988 32) 
("post" 70020 120451) 
("prep" 190472 10) 
("vhea" 190484 36) 
("vmtx" 190520 2716) 
NIL
CL-USER(246): cl-pdf::(progn
                        (remhash (list "ipamincho" (get-encoding *unicode-encoding*)) *font-cache*)
                        (make-c2g-subset (load-ipa-font "../devel/pdf/ipam.ufm" "foo") sample-character-list)
                        (jexample))
#P"tmp.pdf"
CL-USER(248): (file-length "tmp.pdf")
89932

||#
