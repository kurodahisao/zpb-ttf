;;;

(defpackage "ZPB-TTF"
  (:export "OPEN-FONT-LOADER-FROM-STREAM"
           "*DUMP-CHARACTER-LIST*"
           "DUMP-FONT-LOADER-TO-STREAM"))

(defsystem :zpb-patch (:default-pathname #.*default-pathname-defaults*)
  (:parallel "zpb/util.lisp" "zpb/font-loader.lisp" "zpb/cmap.lisp" "zpb/cvt.lisp" "zpb/fpgm.lisp" "zpb/gasp.lisp" "zpb/gdef.lisp" "zpb/gsub.lisp" "zpb/head.lisp" "zpb/hhea.lisp" "zpb/hmtx.lisp" "zpb/loca.lisp" "zpb/maxp.lisp" "zpb/name.lisp" "zpb/os-2.lisp" "zpb/post.lisp" "zpb/util.lisp" "zpb/vhea.lisp" "zpb/vmtx.lisp" "zpb/prep.lisp" "zpb/glyf.lisp" "zpb/font-loader-interface.lisp"))
